const CryptoJS = require("crypto-js");
const crypto = require("diffie-hellman/browser");

export const convertHMS = (value: number) => {
    let hours: any = Math.floor(value / 3600); // get hours
    let minutes: any = Math.floor((value - hours * 3600) / 60); // get minutes
    let seconds: any = value - hours * 3600 - minutes * 60; //  get seconds
    // add 0 if value < 10; Example: 2 => 02
    if (hours < 10) {
        hours = "0" + hours;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    return hours + ":" + minutes + ":" + seconds; // Return is HH : MM : SS
};

export const generateKeys = () => {
    const alice = crypto.createDiffieHellman(1024);
    const bob = crypto.createDiffieHellman(1024);
    alice.generateKeys();
    bob.generateKeys();
    let key_exchange_arguments_alice = {
        prime_number: null, //which is q in our notation
        primitive_root: null, //which is t in our notation
        x_of_a: null, //the only secret parameter for each of its participant
        y_of_a: null, //the public key which each of participant exchange with other part
    };
    let key_exchange_arguments_bob = {
        prime_number: null, //which is q in our notation
        primitive_root: null, //which is t in our notation
        x_of_a: null, //the only secret parameter for each of its participant
        y_of_a: null, //the public key which each of participant exchange with other part
    };
    key_exchange_arguments_alice.prime_number = alice
        .getPrime()
        .toString("hex");
    key_exchange_arguments_alice.primitive_root = alice
        .getGenerator()
        .toString("hex");
    key_exchange_arguments_alice.x_of_a = alice.getPrivateKey().toString("hex");
    key_exchange_arguments_alice.y_of_a = alice.getPublicKey().toString("hex");

    key_exchange_arguments_bob.prime_number = alice.getPrime().toString("hex");
    key_exchange_arguments_bob.primitive_root = alice
        .getGenerator()
        .toString("hex");
    key_exchange_arguments_bob.x_of_a = alice.getPrivateKey().toString("hex");
    key_exchange_arguments_bob.y_of_a = alice.getPublicKey().toString("hex");

    console.log("Alice's public key: ", key_exchange_arguments_alice.y_of_a);
    console.log("Bob's public key: ", key_exchange_arguments_bob.y_of_a);
};

export const aesEncrypt = (message: string) => {
    const ciphertext = CryptoJS.AES.encrypt(
        JSON.stringify({message}),
        "key",
    ).toString();
    return ciphertext;
};

export const aesDecrypt = (ciphertext: string) => {
    const bytes = CryptoJS.AES.decrypt(ciphertext, "key");
    const originalText = bytes.toString(CryptoJS.enc.Utf8);
    const {message} = JSON.parse(originalText);
    return message;
};
